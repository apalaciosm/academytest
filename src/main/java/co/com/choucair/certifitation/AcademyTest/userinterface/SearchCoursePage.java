package co.com.choucair.certifitation.AcademyTest.userinterface;

import  net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class SearchCoursePage extends  PageObject {

    public static final Target UC_BUTTON = Target.the("select universidad choucair")
            .located(By.xpath("//*[@id='universidad']/a/img"));
    public static final Target INPUT_COURSE = Target.the("name of the course to seek")
            .located(By.xpath("//*[@id='coursesearchbox']"));
    public static final Target GO_BUTTON = Target.the("button to seek the course")
            .located(By.xpath("//*[@id='coursesearch']/fieldset/button"));
    public static final Target SELECT_COURSE = Target.the("button to select the course")
            .located(By.xpath("//h4[contains(text(),'Metodología Bancolombia')]"));
    public static final Target NAME_COURSE = Target.the("Name of the selected the course")
            .located(By.xpath("//*[@id='region-main']/div/div[1]/div/div[1]/h3/a"));
}
