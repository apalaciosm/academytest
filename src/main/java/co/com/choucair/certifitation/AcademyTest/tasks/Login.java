package co.com.choucair.certifitation.AcademyTest.tasks;


import co.com.choucair.certifitation.AcademyTest.userinterface.ChoucairAcademyPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.EnterValue;

public class Login implements  Task {
    public static Login OnThePage() {
        return Tasks.instrumented(Login.class);
    }

    @Override
    public  <T extends Actor> void performAs(T actor){
      actor.attemptsTo(Click.on(ChoucairAcademyPage.LOGIN_BUTTON)
      , Enter.theValue("apalaciosm").into(ChoucairAcademyPage.INPUT_USER),
              Enter.theValue("Cl0setome*").into(ChoucairAcademyPage.INPUT_PASSWORD)
              ,Click.on(ChoucairAcademyPage.ENTER_BUTTON)
      );
    }
}
